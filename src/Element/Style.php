<?php
/**
 * Created by PhpStorm.
 * User: wlady2001
 * Date: 18.05.17
 * Time: 11:24
 */

namespace TinyCRM\Element;

/**
 * Class Style
 * @package TinyCRM\Element
 */
trait Style
{
    /**
     * @var array Input config
     */
    protected $config = [
        'group'     => [
            'use'   => false, // wrap the group with div
            'tag'   => '',
            'class' => '',
            'style' => '',
        ],
        'label'     => [
            'use'   => true,
            'empty' => false,
            'class' => '',
            'style' => '',
        ],
        'input'     => [
            'class' => '',
            'style' => '',
        ],
        'container' => [
            'use'   => false, // place input into container
            'tag'   => '',
            'class' => '',
        ],
    ];

    /**
     * @param array $cssConfig
     */
    public function setConfig($cssConfig)
    {
        if (is_string($cssConfig)) {
            $this->config['input']['class'] = $cssConfig;
        } elseif (is_array($cssConfig)) {
            $this->config = array_replace_recursive($this->config, $cssConfig);
        }
    }

    /**
     * @param string $item see self::inputConfig keys
     *
     * @return string 'css="..." attribute or empty
     */
    public function getCssAttribute($item = 'input')
    {
        if (isset($this->config[$item]) && $this->config[$item]['class']) {
            return ' class="' . $this->config[$item]['class'] . '" ';
        }

        return '';
    }

    /**
     * @return string <label> or empty
     */
    public function renderLabel()
    {
        if ($this->config['label']['use'] && $this->getLabel()) {
            return preg_replace('~\s+~', ' ', '<label for="' . $this->getName() . '" ' . $this->getCssAttribute('label') . '>' . ($this->config['label']['empty'] ? '' : $this->getLabel()) . '</label>');
        }

        return '';
    }

    /**
     * @param $type
     *
     * @return string
     */
    public function startContainer($type)
    {
        if ($this->isContainer($type) && $this->config[$type]['tag']) {
            return preg_replace('~\s+~', ' ', '<' . $this->config[$type]['tag'] . ' ' . $this->getCssAttribute($type) . '>');
        }
    }

    /**
     * @param $type
     *
     * @return string
     */
    public function endContainer($type)
    {
        if ($this->isContainer($type) && $this->config[$type]['tag']) {
            return '</' . $this->config[$type]['tag'] . '>';
        }
    }

    /**
     * @param $type
     *
     * @return bool
     */
    public function isContainer($type)
    {
        return (boolean)$this->config[$type]['use'];
    }
}
