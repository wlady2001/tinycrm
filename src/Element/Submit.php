<?php
/**
 * Created by PhpStorm.
 * User: wlady2001
 * Date: 17.05.17
 * Time: 15:34
 */

namespace TinyCRM\Element;

/**
 * Class Submit
 * @package TinyCRM\Element
 */
class Submit extends Element
{
    /**
     * @inheritdoc
     */
    protected function renderInput()
    {
        return '<button type="submit" name="' . $this->getName() . '" ' . $this->getCssAttribute() . '>' . $this->getLabel() . '</button>';
    }
}
