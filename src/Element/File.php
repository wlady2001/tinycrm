<?php
/**
 * Created by PhpStorm.
 * User: wlady2001
 * Date: 17.05.17
 * Time: 20:05
 */

namespace TinyCRM\Element;

/**
 * Class File
 * @package TinyCRM\Element
 */
class File extends Element
{
    /**
     * @inheritdoc
     */
    protected function renderInput()
    {
        return '<input type="file" name="' . $this->getName() . '" ' . $this->getCssAttribute() . '/>';
    }
}
