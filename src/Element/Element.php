<?php
/**
 * Created by PhpStorm.
 * User: wlady2001
 * Date: 17.05.17
 * Time: 15:02
 */

namespace TinyCRM\Element;

/**
 * Class Element
 * @package TinyCRM\Element
 */
abstract class Element
{
    use Style;

    /**
     * @var string Name of this element
     */
    protected $name = null;

    /**
     * @var string Label of this element
     */
    protected $label = null;

    /**
     * @var mixed Data
     */
    protected $value = null;

    /**
     * Element constructor.
     *
     * @param string $name Input name
     * @param string $label Input label
     * @param string/array $cssConfig Input CSS class / Input Config
     */
    public function __construct($name, $label = '', $config = null)
    {
        $this->setName($name);
        if ($label) {
            $this->setLabel($label);
        }
        $this->setConfig($config);

        return $this;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * Generate HTML block including containers, label & input itself
     * @return string
     */
    public function getInputElement()
    {
        $html  = $this->startContainer('group');
        $html .= $this->renderLabel();
        $html .= $this->startContainer('container');
        $html .= preg_replace('~\s+~', ' ', $this->renderInput());
        $html .= $this->endContainer('container');
        $html .= $this->endContainer('group');

        return $html;
    }

    /**
     * Return the form input for this type of element
     * @return string
     */
    abstract protected function renderInput();

}
