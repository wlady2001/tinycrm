<?php
/**
 * Created by PhpStorm.
 * User: wlady2001
 * Date: 17.05.17
 * Time: 15:35
 */

namespace TinyCRM\Element;

/**
 * Class Text
 * @package TinyCRM\Element
 */
class Text extends Element
{
    /**
     * @inheritdoc
     */
    protected function renderInput()
    {
        return '<input type="text" name="' . $this->getName() . '" value="' . $this->getValue() . '" ' . $this->getCssAttribute() . '/>';
    }
}
