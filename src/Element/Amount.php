<?php
/**
 * Created by PhpStorm.
 * User: wlady2001
 * Date: 17.05.17
 * Time: 15:32
 */

namespace TinyCRM\Element;

/**
 * Class Amount
 * @package TinyCRM\Element
 */
class Amount extends Element
{
    /**
     * @inheritdoc
     */
    public function setValue($value)
    {
        $this->value = number_format($value, 2, '.', '');
    }

    /**
     * @inheritdoc
     */
    protected function renderInput()
    {
        return '<input type="number" name="' . $this->getName() . '" value="' . $this->getValue() . '" ' . $this->getCssAttribute() . ' step="0.01"/>';
    }
}
