<?php
/**
 * Created by PhpStorm.
 * User: wlady2001
 * Date: 17.05.17
 * Time: 15:33
 */

namespace TinyCRM\Element;

/**
 * Class Date
 * @package TinyCRM\Element
 */
class Date extends Element
{
    /**
     * @inheritdoc
     */
    public function setValue($value)
    {
        $this->value = date('Y-m-d', strtotime($value));
    }

    /**
     * @inheritdoc
     */
    protected function renderInput()
    {
        return '<input type="date" name="' . $this->getName() . '" value="' . $this->getValue() . '" ' . $this->getCssAttribute() . ' min="' . date('Y-m-d') . '"/>';
    }
}
