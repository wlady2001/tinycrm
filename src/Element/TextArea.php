<?php
/**
 * Created by PhpStorm.
 * User: wlady2001
 * Date: 17.05.17
 * Time: 15:36
 */

namespace TinyCRM\Element;

/**
 * Class TextArea
 * @package TinyCRM\Element
 */
class TextArea extends Element
{
    /**
     * @inheritdoc
     */
    protected function renderInput()
    {
        return '<textarea name="' . $this->getName() . '" ' . $this->getCssAttribute() . ' rows="5">' . $this->getValue() . '</textarea>';
    }
}
