<?php
/**
 * Created by PhpStorm.
 * User: wlady2001
 * Date: 17.05.17
 * Time: 15:03
 */

namespace TinyCRM\Document;

/**
 * Class Template
 * @package TinyCRM\Document
 */
class Template extends \SplObjectStorage
{
    /**
     * @var string Template title
     */
    protected $title = null;

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @codeCoverageIgnore
     */
    public function save()
    {
        return true;
    }
}