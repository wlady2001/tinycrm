<?php
/**
 * Created by PhpStorm.
 * User: wlady2001
 * Date: 17.05.17
 * Time: 15:04
 */

namespace TinyCRM\Document;

/**
 * Class Document
 * @package TinyCRM\Document
 */
class Document
{
    /**
     * @var string Document title
     */
    protected $title = null;

    /**
     * @var Template
     */
    protected $template = null;

    /**
     * Document constructor.
     *
     * @param $title
     */
    public function __construct($title = '')
    {
        if ($title) {
            $this->setTitle($title);
        }

        return $this;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return Template
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param Template $template
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    }

    /**
     * @codeCoverageIgnore
     */
    public function save()
    {
        return true;
    }
}
