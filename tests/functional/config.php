<?php
/**
 * Created by PhpStorm.
 * User: wlady2001
 * Date: 18.05.17
 * Time: 11:50
 */

return [
    'default' => [
        'group'     => [
            'use'   => true,
            'tag'   => 'div',
            'class' => 'form-group',
            'style' => '',
        ],
        'label'     => [
            'use'   => true,
            'empty' => false,
            'class' => 'col-sm-2 control-label',
            'style' => '',
        ],
        'input'     => [
            'class' => 'form-control',
            'style' => '',
        ],
        'container' => [
            'use'   => true,
            'tag'   => 'div',
            'class' => 'col-sm-10',
        ],
    ],
    'btnStyle' => [
        'group'     => [
            'use'   => true,
            'tag'   => 'div',
            'class' => 'form-group',
            'style' => '',
        ],
        'label'     => [
            'use'   => true,
            'empty' => true,
            'class' => 'col-sm-2 control-label',
            'style' => '',
        ],
        'input'     => [
            'class' => 'btn btn-primary',
            'style' => '',
        ],
        'container' => [
            'use'   => true,
            'tag'   => 'div',
            'class' => 'col-sm-10',
        ],
    ],
    'fileStyle' => [
        'group'     => [
            'use'   => true,
            'tag'   => 'div',
            'class' => 'form-group',
            'style' => '',
        ],
        'label'     => [
            'use'   => true,
            'empty' => false,
            'class' => 'col-sm-2 control-label',
            'style' => '',
        ],
        'input'     => [
            'class' => '',
            'style' => '',
        ],
        'container' => [
            'use'   => true,
            'tag'   => 'div',
            'class' => 'col-sm-10',
        ],
    ],
    'narrowStyle' => [
        'group'     => [
            'use'   => true,
            'tag'   => 'div',
            'class' => 'form-group',
            'style' => '',
        ],
        'label'     => [
            'use'   => true,
            'empty' => false,
            'class' => 'col-sm-2 control-label',
            'style' => '',
        ],
        'input'     => [
            'class' => '',
            'style' => '',
        ],
        'container' => [
            'use'   => true,
            'tag'   => 'div',
            'class' => 'col-sm-3',
        ],
    ],
];
