<?php

require __DIR__ . '/../../vendor/autoload.php';
$config = include __DIR__ . '/config.php';

use TinyCRM\Document;
use TinyCRM\Element;

// новый шаблон документа
$template = new Document\Template();
$template->setTitle('Template 1');

// добавить несколько элементов в шаблон
$el1 = new Element\Text('client', 'Клиент', $config['default']);
$el2 = new Element\TextArea('description', 'Описание', $config['default']);
$el3 = new Element\Amount('amount', 'Сумма', $config['narrowStyle']);
$el4 = new Element\Date('end', 'Окончание договора', $config['narrowStyle']);
$el5 = new Element\File('file1', 'Копия паспорта', $config['fileStyle']);
$el6 = new Element\File('file2', 'Решение кредитного комитета', $config['fileStyle']);
$el7 = new Element\Submit('submit', 'Создать', $config['btnStyle']);
// добавить все элементы к шаблону
$template->attachAll([$el1, $el2, $el3, $el4, $el5, $el6, $el7]);
// TODO: не реализовано
$template->save();

// новый документ на основе подготовленного шаблона
$doc = new Document\Document('Кредитный договор №1');
$doc->setTemplate($template);

// установить тестовые значения
$el1->setValue('Новый клиент');
$el2->setValue('Новый кредитный договор');
$el3->setValue(25000);
$el4->setValue(date('Y-m-d', strtotime('+2 months')));
// TODO: не реализовано
$doc->save();


$loader = new Twig_Loader_Filesystem(__DIR__);
$twig   = new Twig_Environment($loader);

echo $twig->render('test.twig', ['document' => $doc]);
