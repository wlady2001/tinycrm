<?php
/**
 * Created by PhpStorm.
 * User: wlady2001
 * Date: 17.05.17
 * Time: 16:37
 */

namespace TinyCRM\Document\Tests;

use PHPUnit\Framework\TestCase;
use TinyCRM\Document\Document;
use TinyCRM\Document\Template;

class DocumentTest extends TestCase
{
    /** @var Document */
    protected $document = null;
    /** @var Template */
    protected $template = null;

    /**
     * @dataProvider dataProvider
     */
    public function testConstructor($data)
    {
        $mock = $this->getMockBuilder(Document::class)
                     ->disableOriginalConstructor()
                     ->setConstructorArgs([$data])
                     ->getMock();

        $mock->expects($this->once())
             ->method('setTitle')
             ->with(
                 $this->equalTo($data)
             );

        $reflectedClass = new \ReflectionClass(Document::class);
        $constructor    = $reflectedClass->getConstructor();
        $constructor->invoke($mock, $data);
    }

    public function testSetTemplate()
    {
        $this->template = new Template();
        $this->document->setTemplate($this->template);
        $this->assertInstanceOf(Template::class, $this->document->getTemplate());
    }

    /**
     * @dataProvider dataProvider
     */
    public function testSetTitle($data)
    {
        $this->document->setTitle($data);
        $this->assertEquals($this->document->getTitle(), $data);
    }

    public function dataProvider()
    {
        return [
            ['Test Document Generated 1'],
            ['Test Document Generated 2'],
        ];
    }

    protected function setUp()
    {
        $this->document = new Document();
    }
}
