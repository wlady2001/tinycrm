<?php
/**
 * Created by PhpStorm.
 * User: wlady2001
 * Date: 17.05.17
 * Time: 16:02
 */

namespace TinyCRM\Document\Tests;

use PHPUnit\Framework\TestCase;
use TinyCRM\Document\Template;

class TemplateTest extends TestCase
{
    /** @var Template */
    protected $template = null;

    /**
     * @dataProvider dataProvider
     *
     * @param $data
     */
    public function testSetTitle($data)
    {
        $this->template->setTitle($data);
        $this->assertEquals($this->template->getTitle(), $data);
    }

    public function dataProvider()
    {
        return [
            ['Test Template Generated 1'],
            ['Test Template Generated 2'],
        ];
    }

    protected function setUp()
    {
        $this->template = new Template();

    }
}
