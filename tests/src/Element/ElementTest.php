<?php
/**
 * Created by PhpStorm.
 * User: wlady2001
 * Date: 17.05.17
 * Time: 17:15
 */

namespace TinyCRM\Element;

use PHPUnit\Framework\TestCase;
use TinyCRM\Document\Template;

class ElementTest extends TestCase
{
    /** @var Element */
    protected $element = null;

    /**
     * @dataProvider dataProvider
     */
    public function testConstructor($name, $label = null, $cssClass = null)
    {
        if (isset($cssClass) && is_array($cssClass)) {
            $this->element = new Text($name, $label, $cssClass);
            $this->assertEquals($this->element->getCssAttribute('input'), ' class="' . $cssClass['input']['class'] . '" ');
        } elseif (isset($cssClass) && is_string($cssClass)) {
            $this->element = new Text($name, $label, $cssClass);
            $this->assertEquals($this->element->getCssAttribute('input'), ' class="' . $cssClass . '" ');
        } elseif (isset($label)) {
            $this->element = new Text($name, $label);
            $this->assertEquals($this->element->getLabel(), $label);
        } else {
            $this->element = new Text($name);
            $this->assertEquals($this->element->getName(), $name);
        }
        $this->assertEmpty($this->element->getCssAttribute('unknown'));
        $template = new Template();
        $template->attachAll([$this->element]);
        $this->assertTrue($template->contains($this->element));
    }

    /**
     * @dataProvider valueProvider
     */
    public function testGetValue($name, $value)
    {
        $this->element = new Text($name);
        $this->element->setValue($value);
        $this->assertEquals($this->element->getValue(), $value);
    }

    /**
     * @dataProvider tagProvider
     */
    public function testGetInputElement($name, $label, $cssClass, $expected)
    {
        $this->element = new Text($name, $label, $cssClass);
        $this->assertEquals($this->element->getInputElement(), $expected);
    }

    public function dataProvider()
    {
        return [
            ['test_element_1'],
            ['test_element_2', 'Test Label 2'],
            ['test_element_3', 'Test Label 3', 'test-class'],
            [
                'test_element_4',
                'Test Label 4',
                [
                    'input' => [
                        'class' => 'test-class',
                    ],
                    'label' => [
                        'empty' => true,
                    ],
                ]
            ],
        ];
    }

    public function valueProvider()
    {
        return [
            ['test_element_1', 'Test Value 1'],
            ['test_element_2', 'Test Value 2'],
            ['test_element_3', 'Test Value 3'],
        ];
    }

    public function tagProvider()
    {
        return [
            [
                'test_element_1',
                'Test Label 1',
                'test-class',
                '<label for="test_element_1" >Test Label 1</label><input type="text" name="test_element_1" value="" class="test-class" />'
            ],
            [
                'test_element_2',
                'Test Label 2',
                [
                    'input' => [
                        'class' => 'test-class-2',
                    ],
                    'label' => [
                        'empty' => true,
                    ],
                ],
                '<label for="test_element_2" ></label><input type="text" name="test_element_2" value="" class="test-class-2" />'
            ],
            [
                'test_element_3',
                'Test Label 3',
                [
                    'group'     => [
                        'use'   => true,
                        'tag'   => 'div',
                        'class' => 'form-group',
                    ],
                    'label' => [
                        'use' => false,
                    ],
                    'container' => [
                        'use'   => true,
                        'tag'   => 'div',
                        'class' => 'container',
                    ],
                ],
                '<div class="form-group" ><div class="container" ><input type="text" name="test_element_3" value="" /></div></div>'
            ],
        ];
    }
}
