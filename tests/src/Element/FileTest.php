<?php
/**
 * Created by PhpStorm.
 * User: wlady2001
 * Date: 17.05.17
 * Time: 20:20
 */

namespace TinyCRM\Element;

use PHPUnit\Framework\TestCase;

class FileTest extends TestCase
{
    /** @var Element */
    protected $element = null;

    /**
     * @dataProvider tagProvider
     */
    public function testSetValue($name, $label, $cssClass, $expected)
    {
        $this->element = new File($name, $label, $cssClass);
        $this->assertEquals($this->element->getInputElement(), $expected);
    }

    public function tagProvider()
    {
        return [
            [
                'test_element_1',
                'Test Label 1',
                'test-class',
                '<label for="test_element_1" >Test Label 1</label><input type="file" name="test_element_1" class="test-class" />'
            ],
            [
                'test_element_2',
                'Test Label 2',
                [
                    'input' => [
                        'class' => 'test-class-2',
                    ],
                    'label' => [
                        'empty' => true,
                    ],
                ],
                '<label for="test_element_2" ></label><input type="file" name="test_element_2" class="test-class-2" />'
            ],
            [
                'test_element_3',
                'Test Label 3',
                [
                    'label' => [
                        'use' => false,
                    ],
                ],
                '<input type="file" name="test_element_3" />'
            ],
        ];
    }
}
