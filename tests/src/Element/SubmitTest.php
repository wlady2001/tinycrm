<?php
/**
 * Created by PhpStorm.
 * User: wlady2001
 * Date: 17.05.17
 * Time: 17:57
 */

namespace TinyCRM\Element;

use PHPUnit\Framework\TestCase;

class SubmitTest extends TestCase
{
    /** @var Element */
    protected $element = null;

    /**
     * @dataProvider tagProvider
     */
    public function testSetValue($name, $label, $cssClass, $expected)
    {
        $this->element = new Submit($name, $label, $cssClass);
        $this->assertEquals($this->element->getInputElement(), $expected);
    }

    public function tagProvider()
    {
        return [
            [
                'test_element_1',
                'Test Label 1',
                'test-class',
                '<label for="test_element_1" >Test Label 1</label><button type="submit" name="test_element_1" class="test-class" >Test Label 1</button>'
            ],
            [
                'test_element_2',
                'Test Label 2',
                [
                    'input' => [
                        'class' => 'test-class-2',
                    ],
                    'label' => [
                        'empty' => true,
                    ],
                ],
                '<label for="test_element_2" ></label><button type="submit" name="test_element_2" class="test-class-2" >Test Label 2</button>'
            ],
            [
                'test_element_3',
                'Test Label 3',
                [
                    'label' => [
                        'use' => false,
                    ],
                ],
                '<button type="submit" name="test_element_3" >Test Label 3</button>'
            ],
        ];
    }

}
