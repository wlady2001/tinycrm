<?php
/**
 * Created by PhpStorm.
 * User: wlady2001
 * Date: 17.05.17
 * Time: 17:57
 */

namespace TinyCRM\Element;

use PHPUnit\Framework\TestCase;

class AmountTest extends TestCase
{
    /** @var Element */
    protected $element = null;

    /**
     * @dataProvider tagProvider
     */
    public function testSetValue($name, $label, $cssClass, $value, $expected)
    {
        $this->element = new Amount($name, $label, $cssClass);
        $this->element->setValue($value);
        $this->assertEquals($this->element->getInputElement(), $expected);
    }

    public function tagProvider()
    {
        return [
            [
                'test_element_1',
                'Test Label 1',
                'test-class',
                1000,
                '<label for="test_element_1" >Test Label 1</label><input type="number" name="test_element_1" value="1000.00" class="test-class" step="0.01"/>'
            ],
            [
                'test_element_2',
                'Test Label 2',
                [
                    'input' => [
                        'class' => 'test-class-2',
                    ],
                    'label' => [
                        'empty' => true,
                    ],
                ],
                2000.0,
                '<label for="test_element_2" ></label><input type="number" name="test_element_2" value="2000.00" class="test-class-2" step="0.01"/>'
            ],
            [
                'test_element_3',
                'Test Label 3',
                [
                    'label' => [
                        'use' => false,
                    ],
                ],
                3000.0000,
                '<input type="number" name="test_element_3" value="3000.00" step="0.01"/>'
            ],
        ];
    }

}
