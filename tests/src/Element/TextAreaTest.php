<?php
/**
 * Created by PhpStorm.
 * User: wlady2001
 * Date: 17.05.17
 * Time: 17:57
 */

namespace TinyCRM\Element;

use PHPUnit\Framework\TestCase;

class TextAreaTest extends TestCase
{
    /** @var Element */
    protected $element = null;

    /**
     * @dataProvider tagProvider
     */
    public function testSetValue($name, $label, $cssClass, $value, $expected)
    {
        $this->element = new TextArea($name, $label, $cssClass);
        $this->element->setValue($value);
        $this->assertEquals($this->element->getInputElement(), $expected);
    }

    public function tagProvider()
    {
        return [
            [
                'test_element_1',
                'Test Label 1',
                'test-class',
                'Test Text 1',
                '<label for="test_element_1" >Test Label 1</label><textarea name="test_element_1" class="test-class" rows="5">Test Text 1</textarea>'
            ],
            [
                'test_element_2',
                'Test Label 2',
                [
                    'input' => [
                        'class' => 'test-class-2',
                    ],
                    'label' => [
                        'empty' => true,
                    ],
                ],
                'Test Text 2',
                '<label for="test_element_2" ></label><textarea name="test_element_2" class="test-class-2" rows="5">Test Text 2</textarea>'
            ],
            [
                'test_element_3',
                'Test Label 3',
                [
                    'label' => [
                        'use' => false,
                    ],
                ],
                '',
                '<textarea name="test_element_3" rows="5"></textarea>'
            ],
        ];
    }

}
