<?php
/**
 * Created by PhpStorm.
 * User: wlady2001
 * Date: 17.05.17
 * Time: 17:57
 */

namespace TinyCRM\Element;

use PHPUnit\Framework\TestCase;

class DateTest extends TestCase
{
    /** @var Element */
    protected $element = null;

    /**
     * @dataProvider tagProvider
     */
    public function testSetValue($name, $label, $cssClass, $value, $expected)
    {
        $this->element = new Date($name, $label, $cssClass);
        $this->element->setValue($value);
        $this->assertEquals($this->element->getInputElement(), $expected);
    }

    public function tagProvider()
    {
        return [
            [
                'test_element_1',
                'Test Label 1',
                'test-class',
                date('Y-m-d', strtotime('+5 days')),
                '<label for="test_element_1" >Test Label 1</label><input type="date" name="test_element_1" value="' . date('Y-m-d',
                    strtotime('+5 days')) . '" class="test-class" min="' . date('Y-m-d') . '"/>'
            ],
            [
                'test_element_2',
                'Test Label 2',
                [
                    'input' => [
                        'class' => 'test-class-2',
                    ],
                    'label' => [
                        'empty' => true,
                    ],
                ],
                date('Y-m-d', strtotime('+2 weeks')),
                '<label for="test_element_2" ></label><input type="date" name="test_element_2" value="' . date('Y-m-d',
                    strtotime('+2 weeks')) . '" class="test-class-2" min="' . date('Y-m-d') . '"/>'
            ],
            [
                'test_element_3',
                'Test Label 3',
                [
                    'label' => [
                        'use' => false,
                    ],
                ],
                date('Y-m-d', strtotime('+6 months')),
                '<input type="date" name="test_element_3" value="' . date('Y-m-d', strtotime('+6 months')) . '" min="' . date('Y-m-d') . '"/>'
            ],
        ];
    }

}
