## TinyCRM 

Каркас для простой системы электронного документооборота.

### Установка

Перед началом работы необходимо установить внешние пакеты. В директории проекта выполните команды:

```sh
composer install
vendor/bin/phpunit
```

Если все установлено правильно будет сгенерирован отчет о покрытии кода,
который можно просмотреть в браузере, открыв файл **tests/report/index.html**
 
Отчет о покрытии кода:
![Отчет о покрытии кода](screenshot-1.png)

### Пример использования
Файл **tests/functional/test.php** - простой функциональный тест пакета TinyCR, генерирующий форму для создания
документа:

```php
<?php

require __DIR__ . '/../../vendor/autoload.php';
$config = include __DIR__ . '/config.php';

use TinyCRM\Document;
use TinyCRM\Element;

// новый шаблон документа
$template = new Document\Template();
$template->setTitle('Template 1');

// добавить несколько элементов в шаблон
$el1 = new Element\Text('client', 'Клиент', $config['default']);
$el2 = new Element\TextArea('description', 'Описание', $config['default']);
$el3 = new Element\Amount('amount', 'Сумма', $config['narrowStyle']);
$el4 = new Element\Date('end', 'Окончание договора', $config['narrowStyle']);
$el5 = new Element\File('file1', 'Копия паспорта', $config['fileStyle']);
$el6 = new Element\File('file2', 'Решение кредитного комитета', $config['fileStyle']);
$el7 = new Element\Submit('submit', 'Создать', $config['btnStyle']);
// добавить все элементы к шаблону
$template->attachAll([$el1, $el2, $el3, $el4, $el5, $el6, $el7]);

// новый документ на основе подготовленного шаблона
$doc = new Document\Document('Кредитный договор №1');
$doc->setTemplate($template);

// установить тестовые значения
$el1->setValue('Новый клиент');
$el2->setValue('Новый кредитный договор');
$el3->setValue(25000);
$el4->setValue(date('Y-m-d', strtotime('+2 months')));

// вывод при помощи Twig
$loader = new Twig_Loader_Filesystem(__DIR__);
$twig = new Twig_Environment($loader);
echo $twig->render('test.twig', ['document' => $doc]);
```

Используемый шаблон Twig:

```html
<html>
<head>
    <title>{{ document.getTitle() }}</title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
</head>
<body>
<div class="container-fluid">
    <div class="container">
        <h2 class="text-center">{{ document.getTitle() }}</h2>
        <form class="form-horizontal">
        {% for slot in document.getTemplate() %}
            {{ slot.getInputElement() | raw }}
        {%  endfor %}
        </form>
    </div>
</div>
</body>
</html>
```

Страница, сгенерированная функциональным тестом:

![Страница, сгенерированная функциональным тестом](screenshot-2.png)
